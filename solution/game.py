from api import init_game, do_move, skipAturn, steal_koalas, freeASpot, bot_vs_bot_game
from minimax import minimax
import time
from uitls import Action , print_matrix, print_nearby

from score import find_moves_to_move

def game():
    game_state = bot_vs_bot_game()
    #time.sleep(1)
    print(game_state[3])
    #[matrix_full,player1,player2,gameId,numOfMove,True]

    while game_state[5]:
        player_turn = True
        #matrix, a_score, depth, turn_player, player1, player2, moves, numOfMove
        
        move = minimax(game_state[0], 0, 3, player_turn, game_state[1], game_state[2], [], game_state[4])
        if len(move[1]) != 0:
            score = move[0]
            move = move[1][0]
        else:
            print(move)
            print(find_moves_to_move(game_state[0], game_state[1], game_state[2], player_turn))
            #print_nearby(game_state[0], game_state[1])
            move = Action("SKIP","203820","a",1,0,0)
            print(game_state[1].x,game_state[1].y, game_state[2].x,game_state[2].y)
            #print_matrix(game_state[0])
        
        print(move.direction,move.distance, score)
        if move.actionType == "MOVE":
            game_state = do_move(game_state[3], move.direction, move.distance)
        elif move.actionType == "SKIP":
            game_state = skipAturn(game_state[3])
        elif move.actionType == "STEAL":
            game_state = steal_koalas(game_state[3])
        #elif move.actionType == "TELEPORT":
        #    teleport()
        elif move.actionType == "FREE A SPOT":
            game_state = freeASpot(game_state[3], move.free_x, move.free_y)

        #time.sleep(1)
        
        
        
			
    
game()