import requests
from uitls import make_matrix, print_matrix, make_player
import json
from score import  find_nearest_king, find_moves_to_move, score
import time

def bot_vs_bot_game():
    #content = requests.get("https://aibg2021.herokuapp.com/botVSbot?player1Id=203820&player2Id=203821")
    content = requests.get("https://aibg2021.herokuapp.com/joinGame?playerId=203820&gameId=4099")
    #https://aibg2021.herokuapp.com/joinGame?playerId=123456&gameId=123
    json_data = json.loads(content.text)
    return get_state(json_data)


def init_game():
    content = requests.get("https://aibg2021.herokuapp.com/makeGame?playerId=203820") #train/makeGame?playerId=203820
    json_data = json.loads(content.text)
    return get_state(json_data)

def do_move(gameId,direction,distance):
    #print('https://aibg2021.herokuapp.com/train/move?playerId=203820&gameId='+str(gameId)+'&direction='+str(direction)+'&distance='+str(distance))
    content = requests.get('https://aibg2021.herokuapp.com/move?playerId=203820&gameId='+str(gameId)+'&direction='+str(direction)+'&distance='+str(distance)) #train/makeGame?playerId=203820
    json_data = json.loads(content.text)
    while "message" in json_data:
        if json_data["message"] == "Game is finished":
            break
        print("one more time")
        content = requests.get('https://aibg2021.herokuapp.com/move?playerId=203820&gameId='+str(gameId)+'&direction='+str(direction)+'&distance='+str(distance)) #train/makeGame?playerId=203820
        json_data = json.loads(content.text)
    return get_state(json_data)

def steal_koalas(gameId):
    
    content = requests.get('https://aibg2021.herokuapp.com/stealKoalas?playerId=203820&gameId='+str(gameId)) #train/makeGame?playerId=203820
    json_data = json.loads(content.text)
    while "message" in json_data:
        if json_data["message"] == "Game is finished":
            break
        print("one more time")
        content = requests.get('https://aibg2021.herokuapp.com/stealKoalas?playerId=203820&gameId='+str(gameId)) #train/makeGame?playerId=203820
        json_data = json.loads(content.text)
    return get_state(json_data)

def skipAturn(gameId):
    content = requests.get('https://aibg2021.herokuapp.com/skipATurn?playerId=203820&gameId='+str(gameId)) #train/makeGame?playerId=203820
    json_data = json.loads(content.text)
    while "message" in json_data:
        if json_data["message"] == "Game is finished":
            break
        print("one more time")
        content = requests.get('https://aibg2021.herokuapp.com/skipATurn?playerId=203820&gameId='+str(gameId)) #train/makeGame?playerId=203820
        json_data = json.loads(content.text)
    return get_state(json_data)

def freeASpot(gameId,x,y):
    content = requests.get('https://aibg2021.herokuapp.com/train/freeASpot?playerId=203820&gameId='+str(gameId)+'&&x='+str(x)+'&&y='+str(y)) #train/makeGame?playerId=203820
    json_data = json.loads(content.text)
    return get_state(json_data)


def get_state(json_data):
    

    
    if "message" in json_data.keys():
        if json_data["message"] == "Game is fineshed":
            return [0,0,0,0,0,False]

    #print(json_data.keys())
    if 'error' in json_data.keys():
        print(json_data['error'])
    gameId = json_data["gameId"]
    if json_data["player1"]["teamName"] == "AddEntropy":
        player1 = json_data["player1"]
        player2 = json_data["player2"]
    else:
        player2 = json_data["player1"]
        player1 = json_data["player2"]
    numOfMove = json_data["numOfMove"]

    maps = json_data["map"]["tiles"]
    matrix_full = make_matrix(maps)
    player1 = make_player(player1)
    player2 = make_player(player2)
    return [matrix_full,player1,player2,gameId,numOfMove,True]
    #       [matrix_full,player1,player2,gameId,numOfMove,True]


def check_if_turn():
    time.sleep(5)



'''
b = requests.get("https://aibg2021.herokuapp.com/train/move?playerId=203820&gameId="+str(gameId)+"&direction=d&distance=1")
c = requests.get("https://aibg2021.herokuapp.com/train/move?playerId=203820&gameId="+str(gameId)+"&direction=e&distance=1")
json_data = json.loads(c.text)
print(json.dumps(c.json(),indent=2))
'''