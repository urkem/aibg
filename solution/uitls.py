class Action:
    def __init__(self, actionType, playerId, direction, distance, free_x, free_y):
        self.playerId = playerId
        self.direction = direction
        self.distance = distance
        self.actionType = actionType
        self.free_x = free_x
        self.free_y = free_y

class Tile:
    def __init__(self, row, column, ownedByTeam, numOfItems, itemType):
        self.row = row
        self.column = column
        self.ownedByTeam = ownedByTeam
        self.numOffItems = numOfItems
        self.itemType = itemType

class Player:
    x: int
    y: int
    score: int
    gathered_koalas: int
    energy: int
    has_free_a_spot: bool
    number_of_used_free_a_spot: int
    num_of_skip_a_turn_used: int
    executed_action: None
    team_name: str
    skin: int

    def __init__(self, x: int, y: int, score: int, gathered_koalas: int, energy: int, has_free_a_spot: bool, number_of_used_free_a_spot: int, num_of_skip_a_turn_used: int, executed_action: None, team_name: str, skin: int) -> None:
        self.x = x
        self.y = y
        self.score = score
        self.gathered_koalas = gathered_koalas
        self.energy = energy
        self.has_free_a_spot = has_free_a_spot
        self.number_of_used_free_a_spot = number_of_used_free_a_spot
        self.num_of_skip_a_turn_used = num_of_skip_a_turn_used
        self.executed_action = executed_action
        self.team_name = team_name
        self.skin = skin

def make_matrix(map):
    matrix = []
    num = -1
    for row in map:
        matrix.append([])
        num +=1
        for tile in row:
            matrix[num].append(Tile(tile['row'],tile['column'],tile['ownedByTeam'],tile['tileContent']['numOfItems'],tile['tileContent']['itemType']))
    #print_matrix(matrix)
    return matrix

def print_matrix(matrix):
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if matrix[i][j].itemType == "EMPTY":
                if matrix[i][j].ownedByTeam == "AddEntropy":
                    print(" *",end="")
                if matrix[i][j].ownedByTeam != "":
                    print(" ?",end="")
                else:
                    print(" -",end="") 
            elif matrix[i][j].itemType == "ENERGY":
                print(" E",end="")
            elif matrix[i][j].itemType == "FREE_A_SPOT":
                print(" ^",end="")
            elif matrix[i][j].itemType == "HOLE":
                print(" 0",end="")
            elif matrix[i][j].itemType == "KOALA":
                print(" K", end="")
            elif matrix[i][j].itemType == "KOALA_CREW":
                print(" B", end="")
            else:
                print(" "+str(matrix[i][j]),end="")
        print("")

def make_player(map):
    #var player_one
    player_one = Player(map['x'], map['y'], map['score'], map['gatheredKoalas'], map['energy'], map['hasFreeASpot'], map['numberOfUsedFreeASpot'], map['numOfSkipATurnUsed'], map['executedAction'], map['teamName'], map['skin'])

    return player_one

def check_win(player1, player2, matrix, numOfMove):
    if player1.num_of_skip_a_turn_used < 5 and player2.num_of_skip_a_turn_used < 5 and check_free_spots(matrix) and numOfMove < 250:
        return True  
    return False

def check_free_spots(matrix):
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if matrix[i][j].ownedByTeam == "" and matrix[i][j].itemType != "HOLE":
                return True
    return False

def find_moves(matrix, player1, player2, turn_player):
    pass

def go_to(player, move, matrix):
    positions_and_matrix = []
    positions_and_matrix.append(player.x)
    positions_and_matrix.append(player.y)
    positions_and_matrix.append(matrix)
    positions_and_matrix.append(player)
    for i in range(move.distance):
        positions_and_matrix = move_to(positions_and_matrix[0], positions_and_matrix[1], move.direction, positions_and_matrix[2], positions_and_matrix[3])
    return positions_and_matrix

def move_to(x, y, direction, matrix, player):
    if matrix[x][y].itemType == "KOALA":
        player.score += 150
        player.gathered_koalas += 1
    elif matrix[x][y].itemType == "ENERGY":
        player.energy += 1
    elif matrix[x][y].itemType == "KOALA_CREW":
        player.score += 1500
    elif matrix[x][y].itemType == "FREE_A_SPOT":
        player.score += 1500
        player.has_free_a_spot = True
    matrix[x][y] = Tile(x, y, "", 1, "EMPTY")
    ret_matrix = []
    if direction == "s":
        ret_matrix.append(x + 2)
        ret_matrix.append(y)
    elif direction == "w":
        ret_matrix.append(x - 2)
        ret_matrix.append(y)
    if x % 2 == 0:
        if direction == "d":
            ret_matrix.append(x +1)
            ret_matrix.append(y)
        elif direction == "q":
            #return [x-1, y-1]
            ret_matrix.append(x - 1)
            ret_matrix.append(y - 1)
        elif direction == "e":
            #return [x-1, y]
            ret_matrix.append(x - 1)
            ret_matrix.append(y)
        elif direction == "a":
            #return [x+1, y-1]
            ret_matrix.append(x + 1)
            ret_matrix.append(y - 1)
    else:
        if direction == "d":
            #return [x+1, y+1]
            ret_matrix.append(x + 1)
            ret_matrix.append(y + 1)
        elif direction == "q":
            #return [x-1, y]
            ret_matrix.append(x - 1)
            ret_matrix.append(y)
        elif direction == "e":
            #return [x-1, y+1]
            ret_matrix.append(x - 1)
            ret_matrix.append(y + 1)
        elif direction == "a":
            #return [x+1, y]
            ret_matrix.append(x + 1)
            ret_matrix.append(y)
    
    ret_matrix.append(matrix)
    ret_matrix.append(player)
    return ret_matrix

def print_nearby(matrix,player):
    x = player.x
    y = player.y

    if x %2 == 0:
        print("s",matrix[x+2][y].ownedByTeam,matrix[x+2][y].itemType)
        print("w",matrix[x-2][y].ownedByTeam,matrix[x-2][y].itemType)
        print("q",matrix[x-1][y-1].ownedByTeam,matrix[x-1][y-1].itemType)
        print("a",matrix[x+1][y-1].ownedByTeam,matrix[x+1][y-1].itemType)
        print("d",matrix[x+1][y].ownedByTeam,matrix[x+1][y].itemType)
        print("e",matrix[x-1][y].ownedByTeam,matrix[x-1][y].itemType)
    else:
        print("s",matrix[x+2][y].ownedByTeam,matrix[x+2][y].itemType)
        print("w",matrix[x-2][y].ownedByTeam,matrix[x-2][y].itemType)
        print("q",matrix[x-1][y].ownedByTeam,matrix[x-1][y].itemType)
        print("a",matrix[x+1][y].ownedByTeam,matrix[x+1][y].itemType)
        print("d",matrix[x+1][y+1].ownedByTeam,matrix[x+1][y+1].itemType)
        print("e",matrix[x-1][y+1].ownedByTeam,matrix[x-1][y+1].itemType)


def go_to_for_teleport(move, player, matrix):
    positions_and_matrix = []
    positions_and_matrix.append(player.x)
    positions_and_matrix.append(player.y)
    positions_and_matrix.append(matrix)
    positions_and_matrix.append(player)
    for i in range(move.distance):
        positions_and_matrix = move_to(positions_and_matrix[0], positions_and_matrix[1], move.direction, positions_and_matrix[2], positions_and_matrix[3])
    return positions_and_matrix

def move_to_for_teleport(x, y, direction, matrix, player):
    if direction == "s":
        return [x + 2, y]
    elif direction == "w":
        return [x - 2, y]
    if x % 2 == 0:
        if direction == "d":
            return [x+1, y]
        elif direction == "q":
            return [x-1, y-1]
        elif direction == "e":
            return [x-1, y]
        elif direction == "a":
            return [x+1, y-1]
    else:
        if direction == "d":
            return [x+1, y+1]
        elif direction == "q":
            return [x-1, y]
        elif direction == "e":
            return [x-1, y+1]
        elif direction == "a":
            return [x+1, y]


#TODO send deepcopy  of player
def teleport(matrix, active_player, inactive_player):
    #TODO probably not necessary
    if not check_free_spots(matrix):
        return []
    positions = []
    radius = 2
    player = active_player
    while len(positions) == 0:
        move = Action("MOVE", 0, "a", radius, 0, 0)
        start_position = go_to_for_teleport(move, player, matrix)
        player.x = start_position[0]
        player.y = start_position[1]
        if valid_position(start_position[0],start_position[1]) and matrix[start_position[0]][start_position[1]].itemType != "HOLE" and matrix[start_position[0]][start_position[1]].ownedByTeam == "":
            positions.append(start_position[0])
            positions.append(start_position[1])
            return positions

        for i in range(1, radius+1):
            move = Action("MOVE", 0, "d", i, 0, 0)
            x_y = go_to_for_teleport(move, player, matrix)
            player.x = x_y[0]
            player.y = x_y[1]
            if valid_position(x_y[0],x_y[1]) and matrix[x_y[0]][x_y[1]].itemType != "HOLE" and matrix[x_y[0]][x_y[1]].ownedByTeam == "":
                positions.append(x_y[0])
                positions.append(x_y[1])
                return positions

        for i in range(1, radius+1):
            move = Action("MOVE", 0, "e", i, 0, 0)
            x_y = go_to_for_teleport(move, player, matrix)
            player.x = x_y[0]
            player.y = x_y[1]
            if valid_position(x_y[0],x_y[1]) and matrix[x_y[0]][x_y[1]].itemType != "HOLE" and matrix[x_y[0]][x_y[1]].ownedByTeam == "":
                positions.append(x_y[0])
                positions.append(x_y[1])
                return positions

        for i in range(1, radius+1):
            move = Action("MOVE", 0, "e", i, 0, 0)
            x_y = go_to_for_teleport(move, player, matrix)
            player.x = x_y[0]
            player.y = x_y[1]
            if valid_position(x_y[0],x_y[1]) and matrix[x_y[0]][x_y[1]].itemType != "HOLE" and matrix[x_y[0]][x_y[1]].ownedByTeam == "":
                positions.append(x_y[0])
                positions.append(x_y[1])
                return positions

        for i in range(1, radius+1):
            move = Action("MOVE", 0, "w", i, 0, 0)
            x_y = go_to_for_teleport(move, player, matrix)
            player.x = x_y[0]
            player.y = x_y[1]
            if valid_position(x_y[0],x_y[1]) and matrix[x_y[0]][x_y[1]].itemType != "HOLE" and matrix[x_y[0]][x_y[1]].ownedByTeam == "":
                positions.append(x_y[0])
                positions.append(x_y[1])
                return positions

        for i in range(1, radius+1):
            move = Action("MOVE", 0, "q", i, 0, 0)
            x_y = go_to_for_teleport(move, player, matrix)
            player.x = x_y[0]
            player.y = x_y[1]
            if valid_position(x_y[0],x_y[1]) and matrix[x_y[0]][x_y[1]].itemType != "HOLE" and matrix[x_y[0]][x_y[1]].ownedByTeam == "":
                positions.append(x_y[0])
                positions.append(x_y[1])
                return positions
        
        for i in range(1, radius+1):
            move = Action("MOVE", 0, "a", i, 0, 0)
            x_y = go_to_for_teleport(move, player, matrix)
            player.x = x_y[0]
            player.y = x_y[1]
            if valid_position(x_y[0],x_y[1]) and matrix[x_y[0]][x_y[1]].itemType != "HOLE" and matrix[x_y[0]][x_y[1]].ownedByTeam == "":
                positions.append(x_y[0])
                positions.append(x_y[1])
                return positions

        for i in range(1, radius):
            move = Action("MOVE", 0, "s", i, 0, 0)
            x_y = go_to_for_teleport(move, player, matrix)
            player.x = x_y[0]
            player.y = x_y[1]
            if valid_position(x_y[0],x_y[1]) and matrix[x_y[0]][x_y[1]].itemType != "HOLE" and matrix[x_y[0]][x_y[1]].ownedByTeam == "":
                positions.append(x_y[0])
                positions.append(x_y[1])
                return positions

        radius += 1

def valid_position(x, y):
    if x >= 0 and x<= 26 and y>= 0 and y<=8:
        return True
    return False 

def non_pass_tile(tile):
    if (tile.itemType == "EMPTY" and tile.ownedByTeam != '') or tile.itemType == "HOLE":
        return True
    else:
        return False


def get_near(player, matrix):
    x = player.x
    y = player.y
    penalty = 0
    if valid_position(x+2,y):
        penalty += 1
    if valid_position(x-2,y):
        penalty += 1
    
    if x %2 == 0:
        if valid_position(x-1,y-1):
            penalty += 1
        if valid_position(x+1,y-1):
            penalty += 1
        if valid_position(x+1,y):
            penalty += 1
        if valid_position(x-1,y):
            penalty += 1
    else:
        if valid_position(x-1,y):
            penalty += 1
        if valid_position(x+1,y):
            penalty += 1
        if valid_position(x+1,y+1):
            penalty += 1
        if valid_position(x-1,y+1):
            penalty += 1
    return penalty
