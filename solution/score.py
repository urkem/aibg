from uitls import valid_position, non_pass_tile
def find_nearest_king(matrix, player1):
    all_kings = []
    for row in range(len(matrix)):
        for col in range(len(matrix[row])):
            #print(matrix[row][col])
            if matrix[row][col].numOffItems > 9:
                king_position = []
                king_position.append(row)
                king_position.append(col)
                all_kings.append(king_position)

    if(len(all_kings) > 0):
        nearest_king = all_kings[0]
        for king in all_kings:
            if abs(player1.x - king[0]) + abs(player1.y - king[1] * 4) < abs(player1.x - nearest_king[0]) + abs(player1.y - nearest_king[1] * 4):
                nearest_king.clear()
                nearest_king.append(king)
        return nearest_king
    else:
        return []

def dist_from(matrix,player):
    x = player.x
    y = player.y
    #for i in range(13):
    #    for j in range(4):
    #       pass

    return abs(x - 13) + abs(y - 4)

def check_enemy(active_player, inactive_player):
    energy_winner = -1
    if active_player.energy-5 >  inactive_player.energy:
        energy_winner = 1
    x = active_player.x
    y = active_player.y

    if (x+2) == inactive_player.x and inactive_player.y == y:
        return 1000*energy_winner
    if (x-2) == inactive_player.x and inactive_player.y == y:
        return 1000*energy_winner

    if x %2 == 0:
        if (x-1) == inactive_player.x and inactive_player.y == (y-1):
            return 1000*energy_winner
        if (x+1) == inactive_player.x and inactive_player.y == (y-1):
            return 1000*energy_winner
        if (x+1) == inactive_player.x and inactive_player.y == y:
            return 1000*energy_winner
        if (x-1) == inactive_player.x and inactive_player.y == y:
            return 1000*energy_winner
    else:
        if (x+1) == inactive_player.x and inactive_player.y == y:
            return 1000*energy_winner
        if (x-1) == inactive_player.x and inactive_player.y == y:
            return 1000*energy_winner
        if (x-1) == inactive_player.x and inactive_player.y == (y+1):
            return 1000*energy_winner
        if (x+1) == inactive_player.x and inactive_player.y == (y+1):
            return 1000*energy_winner
    return 0

def block(player,matrix):
    x = player.x
    y = player.y
    penalty = 0
    if valid_position(x+2,y) and non_pass_tile(matrix[x+2][y]):
        penalty += 1
    if valid_position(x-2,y) and non_pass_tile(matrix[x-2][y]):
        penalty += 1
    
    if x %2 == 0:
        if valid_position(x-1,y-1) and non_pass_tile(matrix[x-1][y-1]):
            penalty += 1
        if valid_position(x+1,y-1) and non_pass_tile(matrix[x+1][y-1]):
            penalty += 1
        if valid_position(x+1,y) and non_pass_tile(matrix[x+1][y]):
            penalty += 1
        if valid_position(x-1,y) and non_pass_tile(matrix[x-1][y]):
            penalty += 1
    else:
        if valid_position(x-1,y) and non_pass_tile(matrix[x-1][y]):
            penalty += 1
        if valid_position(x+1,y) and non_pass_tile(matrix[x+1][y]):
            penalty += 1
        if valid_position(x+1,y+1) and non_pass_tile(matrix[x+1][y+1]):
            penalty += 1
        if valid_position(x-1,y+1) and non_pass_tile(matrix[x-1][y+1]):
            penalty += 1
    
    if penalty > 4:
        return penalty
    else:
        return 0


def score(player1, player2, matrix, numOfMove):
    #tup = find_nearest_king(matrix, player1)
    #print(tup)

    #if numOfMove < 30:
    #    return (player1.score - player2.score) * 10 + player1.energy * 100  
    #elif numOfMove < 44:
    #    return player1.energy * 100
    #print(player1.score)
    #print(player2.score)
    return (player1.score - player2.score) * 10 + (player1.energy - player2.energy) * 10 - block(player1,matrix)*500 - dist_from(matrix,player1)*(1000/(numOfMove * 0.1)) + block(player2,matrix)*100 #+ check_enemy(player1,player2)# + dist_from(matrix,player2)*(500/numOfMove)

    # Add -10.000 if does not have any move left
    # Add 10.000 if enemy is next to you



def find_moves_to_move(matrix, player_one, player_two, turn_player):
    if turn_player:
        player1 = player_one
    else:
        player1 = player_two

    all_moves = []
    energy = 0
    if player1.energy > 2:
        energy = 2
    else:
        energy = player1.energy
    

    # Go Up
    for e in range(1, energy + 1):
        if player1.x - e * 2 >= 0 and matrix[player1.x - e * 2][player1.y].itemType != "HOLE" and matrix[player1.x - e * 2][player1.y].ownedByTeam == "":
            move_free = []
            move_free.append('w')
            move_free.append(e)
            move_free.append(player1.x - e * 2)
            move_free.append(player1.y)
            all_moves.append(move_free)
        else:
            break

    # Go Down
    for e in range(1, energy + 1):
        if player1.x + e * 2 < 27 and matrix[player1.x + e * 2][player1.y].itemType != "HOLE" and matrix[player1.x + e * 2][player1.y].ownedByTeam == "":
            move_free = []
            move_free.append('s')
            move_free.append(e)
            move_free.append(player1.x + e * 2)
            move_free.append(player1.y)
            all_moves.append(move_free)
        else:
            break

    # Go Q - Gore levo
    k = 0
    for e in range(1, energy + 1):
        if (player1.x - e) % 2 != 0:
            k += 1
            if player1.x - e >= 0 and player1.y - k >= 0 and matrix[player1.x - e][player1.y - k].itemType != "HOLE" and matrix[player1.x - e][player1.y - k].ownedByTeam == "":
                move_free = []
                move_free.append('q')
                move_free.append(e)
                move_free.append(player1.x - e)
                move_free.append(player1.y - k)
                all_moves.append(move_free)
            else:
                break
        else:
            if player1.x - e >= 0 and player1.y - k >= 0 and matrix[player1.x - e][player1.y - k].itemType != "HOLE" and matrix[player1.x - e][player1.y - k].ownedByTeam == "":
                move_free = []
                move_free.append('q')
                move_free.append(e)
                move_free.append(player1.x - e)
                move_free.append(player1.y - k)
                all_moves.append(move_free)
            else:
                break

    # Go D - Dole desno
    k1 = 0
    for e in range(1, energy + 1):
        if (player1.x + e) % 2 != 0:
            if player1.x + e < 27 and player1.y + k1 < 9 and matrix[player1.x + e][player1.y + k1].itemType != "HOLE" and matrix[player1.x + e][player1.y + k1].ownedByTeam == "":
                move_free = []
                move_free.append('d')
                move_free.append(e)
                move_free.append(player1.x + e)
                move_free.append(player1.y + k1)
                all_moves.append(move_free)
            else:
                break
        else:
            k1 += 1
            if player1.x + e < 27 and player1.y + k1 < 9 and matrix[player1.x + e][player1.y + k1].itemType != "HOLE" and matrix[player1.x + e][player1.y + k1].ownedByTeam == "":
                move_free = []
                move_free.append('d')
                move_free.append(e)
                move_free.append(player1.x + e)
                move_free.append(player1.y + k1)
                all_moves.append(move_free)
            else:
                break

    # Go E - Gore desno
    k2 = 0
    for e in range(1, energy + 1):
        if (player1.x - e) % 2 != 0:
            if player1.x - e >=0 and player1.y + k2 < 9 and matrix[player1.x - e][player1.y + k2].itemType != "HOLE" and matrix[player1.x - e][player1.y + k2].ownedByTeam == "":
                move_free = []
                move_free.append('e')
                move_free.append(e)
                move_free.append(player1.x - e)
                move_free.append(player1.y + k2)
                all_moves.append(move_free)
            else:
                break
        else:
            k2 += 1
            if player1.x - e >=0 and player1.y + k2 < 9 and matrix[player1.x - e][player1.y + k2].itemType != "HOLE" and matrix[player1.x - e][player1.y + k2].ownedByTeam == "":
                move_free = []
                move_free.append('e')
                move_free.append(e)
                move_free.append(player1.x - e)
                move_free.append(player1.y + k2)
                all_moves.append(move_free)
            else:
                break

    # Go A - Dole levo
    k3 = 0
    for e in range(1, energy + 1):
        if (player1.x + e) % 2 != 0:
            k3 += 1
            if player1.x + e < 27 and player1.y - k3 >= 0 and matrix[player1.x + e][player1.y - k3].itemType != "HOLE" and matrix[player1.x + e][player1.y - k3].ownedByTeam == "":
                move_free = []
                move_free.append('a')
                move_free.append(e)
                move_free.append(player1.x + e)
                move_free.append(player1.y - k3)
                all_moves.append(move_free)
            else:
                break
        else:
            if player1.x + e < 27 and player1.y - k3 >= 0 and matrix[player1.x + e][player1.y - k3].itemType != "HOLE" and matrix[player1.x + e][player1.y - k3].ownedByTeam == "":
                move_free = []
                move_free.append('a')
                move_free.append(e)
                move_free.append(player1.x + e)
                move_free.append(player1.y - k3)
                all_moves.append(move_free)
            else:
                break

    #print("all")
    #print(all_moves)
    return all_moves
        #if player1.x - e >= 0 and matrix[x - e][y] : 
    
