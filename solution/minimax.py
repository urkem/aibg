from uitls import check_win, Tile, go_to, Action, teleport , valid_position, get_near
import copy
from score import  find_moves_to_move, score, check_enemy


def minimax(matrix, a_score, depth, turn_player, player1, player2, moves, numOfMove):

	if depth == 0: # or not check_win(player1, player2, matrix, numOfMove)
		return [score(player1, player2, matrix, numOfMove), moves]
		
	#vision = get_vision(matrix, turn_player, player1, player2)

	if turn_player:
		maxEval = [-999900, moves]
		for child in find_moves(matrix, player1, player2, turn_player, numOfMove):
			#print(child[0],child[1])
			#print("1234")
			#child = Action("MOVE","203820",child[0],child[1],0,0)
			#print(find_moves(matrix, player1, player2, turn_player))
			new_moves = copy.deepcopy(moves)
			new_moves.append(child)
			
			next_step = game_generate_new_matrix(copy.deepcopy(matrix), turn_player, child, copy.deepcopy(player1),  copy.deepcopy(player2))
			matrix_gen = next_step[0]
			acc_score = 0
			next_step_player1 = next_step[2]
			next_step_player2 = next_step[3]
			
			#matrix, a_score, depth, turn_player, player1, player2, moves, numOfMove
			eval_comp = minimax(copy.deepcopy(matrix_gen), a_score + acc_score, depth - 1, False, next_step_player1, next_step_player2, new_moves, numOfMove+1)
			
			if eval_comp[0] > maxEval[0]:
				maxEval = eval_comp
		return maxEval
 
	else:
		avalable_moves = find_moves(matrix, player1, player2, turn_player, numOfMove)
		if len(avalable_moves) == 0:
			return [score(player1, player2, matrix, numOfMove), moves]
		
		minEval = [999900, moves]
		
		for child in avalable_moves:

			#child = Action("MOVE","203820",child[0],child[1],0,0)

			new_moves = copy.deepcopy(moves)
			new_moves.append(child)
			
			next_step = game_generate_new_matrix(copy.deepcopy(matrix), turn_player,child, copy.deepcopy(player1),  copy.deepcopy(player2))
			matrix_gen = next_step[0]
			acc_score = 0
			next_step_player1 = next_step[2]
			next_step_player2 = next_step[3]
			
			eval_comp = minimax(copy.deepcopy(matrix_gen), a_score + acc_score, depth - 1, True, next_step_player1, next_step_player2, new_moves, numOfMove+1)
			
			
			if eval_comp[0] < minEval[0]:
				minEval = eval_comp
				#print("minEval",depth,minEval)
			
		return minEval
	

def game_generate_new_matrix(matrix, turn_player, move, player1, player2):
	if turn_player:
		active_player = player1
		inactive_player = player2
	else:
		active_player = player2
		inactive_player = player1

	acc_score = 0
	if move.actionType == "MOVE":
		matrix[active_player.x][active_player.y] = Tile(active_player.x, active_player.y, active_player.team_name, 1, 'EMPTY')

		pos = go_to(active_player, move, matrix)
		if matrix[pos[0]][pos[1]].itemType == "KOALA_CREW":
			active_player.score += 15000

		elif matrix[pos[0]][pos[1]].itemType == "KOALA":
			active_player.score += 150
		
		elif matrix[pos[0]][pos[1]].itemType == "FREE_A_SPOT":
			active_player.score += 15000
		
		elif matrix[pos[0]][pos[1]].itemType == "ENERGY":
			active_player.energy += 1
		
		matrix[pos[0]][pos[1]] = Tile(active_player.x, active_player.y, active_player.team_name, 1, 'EMPTY')
		active_player.score += 100
		active_player.energy -= move.distance
		active_player.x = pos[0]
		active_player.y = pos[1]
	elif move.actionType == "SKIP":
		active_player.energy += 3
	elif move.actionType == "STEAL":
		#print("steeeal")
		active_player.energy -= 5
		if inactive_player.gathered_koalas <= 10:
			active_player.gathered_koalas += inactive_player.gathered_koalas
			active_player.score += 150*inactive_player.gathered_koalas
			inactive_player.score -= 150*inactive_player.gathered_koalas
			inactive_player.gathered_koalas = 0
		else:
			active_player.score += 150*10
			inactive_player.score -= 150*10
			active_player.gathered_koalas += 10
			inactive_player.gathered_koalas -= 10
	elif move.actionType == "TELEPORT":
		#TODO
		position = teleport(matrix, active_player.deepcopy(), inactive_player.deepcopy())
		active_player.score -= 500
		active_player.x = position[0]
		active_player.y = position[1]
		matrix[position[0]][position[1]].ownedByTeam = active_player.teamName
		matrix[position[0]][position[1]].itemType = "EMPTY"
		#stavi se na empty i zauzme se 
	elif move.actionType == "FREE A SPOT":
		#TODO check if active_player traped
		active_player.number_of_used_free_a_spot += 1
		active_player.energy -= 3*active_player.number_of_used_free_a_spot
		if matrix[move.free_x][move.free_y].ownedByTeam == active_player.team_name:
			active_player.score -= 100
		else:
			active_player.score += 100
			inactive_player.score -= 100
		matrix[move.free_x][move.free_y] = Tile(move.free_x, move.free_y, '', 1, 'EMPTY')
	# [matrix, acc_score , player1, player2]
	active_player.energy += 1
	return [matrix, acc_score , player1, player2]



def find_moves(matrix, player1, player2, turn_player, numOfMove):
	if turn_player:
		active_player = player1
		inactive_player = player2
	else:
		active_player = player2
		inactive_player = player1
	

	ret_moves_list = find_moves_to_move(matrix, player1, player2, turn_player)

	ret_moves = []

	if check_enemy(active_player, inactive_player) != 0 and len(ret_moves_list)>0 and active_player.energy >= 5:
		ret_moves.append(Action("STEAL",0, 0, 0, 0, 0))
		return ret_moves

	if len(ret_moves_list) == 1 and numOfMove > 125 and active_player.num_of_skip_a_turn_used < 5:
		#ret_moves_list.pop(0)
		ret_moves.append(Action("SKIP",0,0,0,1,1))
		return ret_moves
	
	if len(ret_moves_list) != 0:
		for child in ret_moves_list:
			if type(child) != list:
				print(child)
			ret_moves.append(Action("MOVE","203820",child[0],child[1],0,0))
		return ret_moves
	else:
		return []

